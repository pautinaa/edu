const reverseInt = (num) => {

  let str = num.toString();
  let res = '';
  
  for (let i = str.length -1; i >= 0; i--) {
    
    if (str[i] !== '0' ) {
      
       if (str[i] === '-' ) {
          return -res;
        }  
      res += str[i];
    }       
   }
    return res;
}
console.log(reverseInt(123));
console.log(reverseInt(-123));
console.log(reverseInt(98098));
console.log(reverseInt(-98098));