const DnaToRna = (dna) =>{
    let str = dna.length;
    let res = '';

    if (dna === '') {
        return '';
    }

    for (let i = 0; i <= str; i++) {

        if (dna[i] === 'G' ) {
            res += 'C';
        } else if (dna[i] === 'C' ) {
            res += 'G';
        } else if (dna[i] === 'T' ) {
            res += 'A';
        } else if (dna[i] === 'A' ) {
            res += 'U';
        }

        else if (dna.split(dna[i]).length > 1)  {
            return null;
        }

    }
    return res;
}

console.log(DnaToRna('AAAAACCC')); // 'UGCACCAGAAUU'
console.log(DnaToRna('CCGTA')); // 'GGCAU'
console.log(DnaToRna('')); // ''
console.log(DnaToRna('AANN')); // null